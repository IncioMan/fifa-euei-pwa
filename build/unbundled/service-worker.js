/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren */
'use strict';





/* eslint-disable quotes, comma-spacing */
var PrecacheConfig = [["/public/bower_components/app-layout/app-drawer-layout/app-drawer-layout.html","a7474857c8dd25d6a9e5aabe763ec01f"],["/public/bower_components/app-layout/app-drawer/app-drawer.html","e9076e309f6194958ac1dd3894a3e371"],["/public/bower_components/app-layout/app-header-layout/app-header-layout.html","0916d57aa30b3c1b54946399c8c01f04"],["/public/bower_components/app-layout/app-header/app-header.html","f5c92182d2100c582b099fdba4b1267f"],["/public/bower_components/app-layout/app-scroll-effects/app-scroll-effects-behavior.html","3007817ffcb7fd1cf6528305c0fec9ff"],["/public/bower_components/app-layout/app-scroll-effects/app-scroll-effects.html","1eaa9e05144414be49e4ee23e16ceca2"],["/public/bower_components/app-layout/app-scroll-effects/effects/blend-background.html","4723ce7f6429640a812ad866ddddb719"],["/public/bower_components/app-layout/app-scroll-effects/effects/fade-background.html","3929482c600a21680def557965850501"],["/public/bower_components/app-layout/app-scroll-effects/effects/material.html","271fe6e745f1a9581a6e01cb3aadce21"],["/public/bower_components/app-layout/app-scroll-effects/effects/parallax-background.html","391d025dcffee3f042c9d2bdd83be377"],["/public/bower_components/app-layout/app-scroll-effects/effects/resize-snapped-title.html","4f3bc3dee2d48426998c6e4425df2b14"],["/public/bower_components/app-layout/app-scroll-effects/effects/resize-title.html","a78d5b787591fb1728631fc5e087cd1c"],["/public/bower_components/app-layout/app-scroll-effects/effects/waterfall.html","6cbd757de769cd5af213aaebb8780745"],["/public/bower_components/app-layout/app-toolbar/app-toolbar.html","bde0a79d3265bef565ee86c699b6bbae"],["/public/bower_components/app-layout/helpers/helpers.html","6a9eeb74102bb1e3ba297c47c441f869"],["/public/bower_components/app-route/app-location.html","0c082f44abb664591f5b99e57a3855e6"],["/public/bower_components/app-route/app-route-converter-behavior.html","3b85a02b434cbccdcb87396be3554258"],["/public/bower_components/app-route/app-route.html","f3550c2eff5c339841d95e47c26dfba6"],["/public/bower_components/app-storage/app-network-status-behavior.html","da5ca8e23c23932448b728ee17c1234e"],["/public/bower_components/firebase/firebase-app.js","e9609563b1a32c207680d3d74e9106bb"],["/public/bower_components/firebase/firebase-auth.js","8e7025e6c5e446318be7bcfb483c3884"],["/public/bower_components/font-roboto/roboto.html","09500fd5adfad056ff5aa05e2aae0ec5"],["/public/bower_components/highcharts-chart/highcharts-behavior.html","6d8cb32d24a399ba1a9ad1b80122c06b"],["/public/bower_components/highcharts-chart/highcharts-chart.html","4e647c32e3358a036fde29f72b590e7a"],["/public/bower_components/highcharts-chart/highcharts-shared-styles.html","d0d49b751e59b42852a867d6af914dcd"],["/public/bower_components/highcharts/highstock.js","d4162ab262156ae81bed45d3262532ac"],["/public/bower_components/highcharts/modules/drilldown.js","277feca2296f4eb6849fca7f5a3bd0d3"],["/public/bower_components/highcharts/modules/exporting.js","e977f783a511bd956761cbda837f6b31"],["/public/bower_components/iron-a11y-announcer/iron-a11y-announcer.html","a3bd031e39dde38cb8e619f670ee50f7"],["/public/bower_components/iron-a11y-keys-behavior/iron-a11y-keys-behavior.html","dd1c0a057fb6866a08d42c8ac4bdce51"],["/public/bower_components/iron-ajax/iron-ajax.html","6fe26e77115b2757c9db65a71153509c"],["/public/bower_components/iron-ajax/iron-request.html","aceaed402f97b77d162ebe36aa070b94"],["/public/bower_components/iron-behaviors/iron-button-state.html","6565a80d1af09299c1201f8286849c3b"],["/public/bower_components/iron-behaviors/iron-control-state.html","1c12ee539b1dbbd0957ae26b3549cc13"],["/public/bower_components/iron-fit-behavior/iron-fit-behavior.html","e048e061e5752630ff76e4597db84d22"],["/public/bower_components/iron-flex-layout/iron-flex-layout-classes.html","324bc9cab816a57fc0476eaf08f88a4c"],["/public/bower_components/iron-flex-layout/iron-flex-layout.html","be17bfc442cd8270b7dec1bb39051621"],["/public/bower_components/iron-icon/iron-icon.html","f2a0dfd0b79864b4f4efb578417a3fef"],["/public/bower_components/iron-icons/iron-icons.html","224dfbc53cf908d9ae68da954ca2d8e9"],["/public/bower_components/iron-icons/social-icons.html","ee8983a2f719e9fd83620dd5a905187b"],["/public/bower_components/iron-iconset-svg/iron-iconset-svg.html","8d0d7213b8c3325ca7e5a658ca9aaf17"],["/public/bower_components/iron-location/iron-location.html","8a4f71232319aed73115b2e356c19fde"],["/public/bower_components/iron-location/iron-query-params.html","32a96be5536aea89a925d16146cb7015"],["/public/bower_components/iron-media-query/iron-media-query.html","7436f9608ebd2d31e4b346921651f84b"],["/public/bower_components/iron-meta/iron-meta.html","dd4ef14e09c5771e70292d70963f6718"],["/public/bower_components/iron-overlay-behavior/iron-focusables-helper.html","6f9ea0b495a22d3c4c51773a1b94e19e"],["/public/bower_components/iron-overlay-behavior/iron-overlay-backdrop.html","35013b4b97041ed6b63cf95dbb9fbcb4"],["/public/bower_components/iron-overlay-behavior/iron-overlay-behavior.html","9fbea74a74d9c9b1a5e38b2059f6f752"],["/public/bower_components/iron-overlay-behavior/iron-overlay-manager.html","707308044cec15e2d3c85cd28d152e89"],["/public/bower_components/iron-pages/iron-pages.html","5872a2ad58225c94b14ddea747f67cbd"],["/public/bower_components/iron-resizable-behavior/iron-resizable-behavior.html","2af69f71643289776a629ce1ff3e244e"],["/public/bower_components/iron-scroll-target-behavior/iron-scroll-target-behavior.html","d0eb39331820f58f3cdcebaa0eed0209"],["/public/bower_components/iron-selector/iron-multi-selectable.html","91eeea310058c0a1b837b685922c03a5"],["/public/bower_components/iron-selector/iron-selectable.html","65b04f3f5f1b551d91a82b36f916f6b6"],["/public/bower_components/iron-selector/iron-selection.html","83545b7d1eae4020594969e6b9790b65"],["/public/bower_components/iron-selector/iron-selector.html","4d2657550768bec0788eba5190cddc66"],["/public/bower_components/jquery/dist/jquery.min.js","0bf02de55b8071e113eb5f1502ec502f"],["/public/bower_components/paper-behaviors/paper-button-behavior.html","edddd3f97cf3ea944f3a48b4154939e8"],["/public/bower_components/paper-behaviors/paper-inky-focus-behavior.html","51a1c5ccd2aae4c1a0258680dcb3e1ea"],["/public/bower_components/paper-behaviors/paper-ripple-behavior.html","b6ee8dd59ffb46ca57e81311abd2eca0"],["/public/bower_components/paper-button/paper-button.html","3f061a077ee938fac479622156071296"],["/public/bower_components/paper-fab/paper-fab.html","0b58f095f983bb4ff0557662d7978aaa"],["/public/bower_components/paper-icon-button/paper-icon-button.html","2a75de00f858ae1e894ab21344464787"],["/public/bower_components/paper-material/paper-material-shared-styles.html","d0eeeb696e55702f3a38ef1ad0058f59"],["/public/bower_components/paper-ripple/paper-ripple.html","417d76dccb679ca1000ac8ca18be4968"],["/public/bower_components/paper-spinner/paper-spinner-behavior.html","7e58a415d4a9b43fbef2651029d61ad0"],["/public/bower_components/paper-spinner/paper-spinner-styles.html","a2122d2c0f3ac98e6911160d8886d31a"],["/public/bower_components/paper-spinner/paper-spinner.html","940e64bbde54dad918d0f5c0e247a8bd"],["/public/bower_components/paper-styles/color.html","430305db311431da78c0a6e1236f9ebe"],["/public/bower_components/paper-styles/default-theme.html","c910188e898624eb890898418de20ee0"],["/public/bower_components/paper-styles/shadow.html","fc449492f51292636b499bc5d7b9b036"],["/public/bower_components/paper-styles/typography.html","bdd7f31bb85f116ce97061c4135b74c9"],["/public/bower_components/paper-toast/paper-toast.html","f64d10724104f3751cae8b764aef56ff"],["/public/bower_components/polymer/polymer-micro.html","69764897a10bacd35c6d3bbe94b6813d"],["/public/bower_components/polymer/polymer-mini.html","e1d2a48eca579ef566846aaa926ea2a5"],["/public/bower_components/polymer/polymer.html","c4a69c37c194b702ee517a3b55f51e33"],["/public/bower_components/polymerfire/firebase-app-script.html","df1897d11acb9c75522859d372873358"],["/public/bower_components/polymerfire/firebase-app.html","29b84490aa6ef31291e6919ca64917fc"],["/public/bower_components/polymerfire/firebase-auth-script.html","12fe480c116018252246dd4366d1a1ef"],["/public/bower_components/polymerfire/firebase-auth.html","c72fcac5475f05e67fbb5966c2a02dc8"],["/public/bower_components/polymerfire/firebase-common-behavior.html","6b5e1fc979b58beb5f548ddd2c751977"],["/public/bower_components/promise-polyfill/Promise.js","543d053e97b2f3bf434212aa4db1fb2a"],["/public/bower_components/promise-polyfill/promise-polyfill-lite.html","06470312beff013fc54695e0bdda2cb3"],["/public/bower_components/webcomponentsjs/webcomponents-lite.min.js","06b0275e1955eb468cc60a3742525970"],["/public/comment-matches.html","efed2d4061d4c5f23d50a28b23a4277f"],["/public/index.html","22966b8d741cb918d3f6d263951ded7a"],["/public/my-app.html","c653df56d4ac4c3090ab3f990c71a8a4"],["/public/my-login.html","51ffed9996b911583c40037c17faa74f"],["/public/my-matches.html","343b264cb5b935b841d3d02289d74975"],["/public/user-app-layout.html","a05ec70097293dfbca0bfaab18778527"]];
/* eslint-enable quotes, comma-spacing */
var CacheNamePrefix = 'sw-precache-v1--' + (self.registration ? self.registration.scope : '') + '-';


var IgnoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var getCacheBustedUrl = function (url, param) {
    param = param || Date.now();

    var urlWithCacheBusting = new URL(url);
    urlWithCacheBusting.search += (urlWithCacheBusting.search ? '&' : '') +
      'sw-precache=' + param;

    return urlWithCacheBusting.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var populateCurrentCacheNames = function (precacheConfig,
    cacheNamePrefix, baseUrl) {
    var absoluteUrlToCacheName = {};
    var currentCacheNamesToAbsoluteUrl = {};

    precacheConfig.forEach(function(cacheOption) {
      var absoluteUrl = new URL(cacheOption[0], baseUrl).toString();
      var cacheName = cacheNamePrefix + absoluteUrl + '-' + cacheOption[1];
      currentCacheNamesToAbsoluteUrl[cacheName] = absoluteUrl;
      absoluteUrlToCacheName[absoluteUrl] = cacheName;
    });

    return {
      absoluteUrlToCacheName: absoluteUrlToCacheName,
      currentCacheNamesToAbsoluteUrl: currentCacheNamesToAbsoluteUrl
    };
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var mappings = populateCurrentCacheNames(PrecacheConfig, CacheNamePrefix, self.location);
var AbsoluteUrlToCacheName = mappings.absoluteUrlToCacheName;
var CurrentCacheNamesToAbsoluteUrl = mappings.currentCacheNamesToAbsoluteUrl;

function deleteAllCaches() {
  return caches.keys().then(function(cacheNames) {
    return Promise.all(
      cacheNames.map(function(cacheName) {
        return caches.delete(cacheName);
      })
    );
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    // Take a look at each of the cache names we expect for this version.
    Promise.all(Object.keys(CurrentCacheNamesToAbsoluteUrl).map(function(cacheName) {
      return caches.open(cacheName).then(function(cache) {
        // Get a list of all the entries in the specific named cache.
        // For caches that are already populated for a given version of a
        // resource, there should be 1 entry.
        return cache.keys().then(function(keys) {
          // If there are 0 entries, either because this is a brand new version
          // of a resource or because the install step was interrupted the
          // last time it ran, then we need to populate the cache.
          if (keys.length === 0) {
            // Use the last bit of the cache name, which contains the hash,
            // as the cache-busting parameter.
            // See https://github.com/GoogleChrome/sw-precache/issues/100
            var cacheBustParam = cacheName.split('-').pop();
            var urlWithCacheBusting = getCacheBustedUrl(
              CurrentCacheNamesToAbsoluteUrl[cacheName], cacheBustParam);

            var request = new Request(urlWithCacheBusting,
              {credentials: 'same-origin'});
            return fetch(request).then(function(response) {
              if (response.ok) {
                return cache.put(CurrentCacheNamesToAbsoluteUrl[cacheName],
                  response);
              }

              console.error('Request for %s returned a response status %d, ' +
                'so not attempting to cache it.',
                urlWithCacheBusting, response.status);
              // Get rid of the empty cache if we can't add a successful response to it.
              return caches.delete(cacheName);
            });
          }
        });
      });
    })).then(function() {
      return caches.keys().then(function(allCacheNames) {
        return Promise.all(allCacheNames.filter(function(cacheName) {
          return cacheName.indexOf(CacheNamePrefix) === 0 &&
            !(cacheName in CurrentCacheNamesToAbsoluteUrl);
          }).map(function(cacheName) {
            return caches.delete(cacheName);
          })
        );
      });
    }).then(function() {
      if (typeof self.skipWaiting === 'function') {
        // Force the SW to transition from installing -> active state
        self.skipWaiting();
      }
    })
  );
});

if (self.clients && (typeof self.clients.claim === 'function')) {
  self.addEventListener('activate', function(event) {
    event.waitUntil(self.clients.claim());
  });
}

self.addEventListener('message', function(event) {
  if (event.data.command === 'delete_all') {
    console.log('About to delete all caches...');
    deleteAllCaches().then(function() {
      console.log('Caches deleted.');
      event.ports[0].postMessage({
        error: null
      });
    }).catch(function(error) {
      console.log('Caches not deleted:', error);
      event.ports[0].postMessage({
        error: error
      });
    });
  }
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    var urlWithoutIgnoredParameters = stripIgnoredUrlParameters(event.request.url,
      IgnoreUrlParametersMatching);

    var cacheName = AbsoluteUrlToCacheName[urlWithoutIgnoredParameters];
    var directoryIndex = 'index.html';
    if (!cacheName && directoryIndex) {
      urlWithoutIgnoredParameters = addDirectoryIndex(urlWithoutIgnoredParameters, directoryIndex);
      cacheName = AbsoluteUrlToCacheName[urlWithoutIgnoredParameters];
    }

    var navigateFallback = '';
    // Ideally, this would check for event.request.mode === 'navigate', but that is not widely
    // supported yet:
    // https://code.google.com/p/chromium/issues/detail?id=540967
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1209081
    if (!cacheName && navigateFallback && event.request.headers.has('accept') &&
        event.request.headers.get('accept').includes('text/html') &&
        /* eslint-disable quotes, comma-spacing */
        isPathWhitelisted([], event.request.url)) {
        /* eslint-enable quotes, comma-spacing */
      var navigateFallbackUrl = new URL(navigateFallback, self.location);
      cacheName = AbsoluteUrlToCacheName[navigateFallbackUrl.toString()];
    }

    if (cacheName) {
      event.respondWith(
        // Rely on the fact that each cache we manage should only have one entry, and return that.
        caches.open(cacheName).then(function(cache) {
          return cache.keys().then(function(keys) {
            return cache.match(keys[0]).then(function(response) {
              if (response) {
                return response;
              }
              // If for some reason the response was deleted from the cache,
              // raise and exception and fall back to the fetch() triggered in the catch().
              throw Error('The cache ' + cacheName + ' is empty.');
            });
          });
        }).catch(function(e) {
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});




